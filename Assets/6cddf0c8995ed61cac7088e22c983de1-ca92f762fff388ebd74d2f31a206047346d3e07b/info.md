# Wide Outlines Renderer Feature for URP and ECS/DOTS/Hybrid Renderer

![outline_shader](https://user-images.githubusercontent.com/2761011/160294277-7ce2243f-c40c-41dd-8a7f-19ed96d10f3d.gif)

## Usage
1. Create a new Unity game object layer for the objects you wish to outline.
2. In your URP asset, turn on MSAA or disable the Depth Texture.
3. In the ForwardRendererData asset of your URP asset, add the renderer feature named "Outline Feature"
4. Set the Layer Mask to the layer created in step 1.
5. Add the objects you wish to outline to the outline layer (this can be done at runtime in ECS by setting the layer of the RenderMesh).
6. Adjust the color and width of the outlines as desired in the renderer feature settings.

## Notes
When I first added support for the Hyrbid Renderer, render layers were not working correctly. This required the use of game object layers to filter out the outlined objects. This might be fixed now, but I have not tested it yet.

## Credits
- The technique for these wide outlines comes from [Ben Golus](https://bgolus.medium.com/) which is described and implemented in [this article](https://bgolus.medium.com/the-quest-for-very-wide-outlines-ba82ed442cd9).
- [Alexander Ameye](https://alexanderameye.github.io/) created the renderer feature and shader modifications to make this work in URP, original shared [here](https://twitter.com/alexanderameye/status/1332286868222775298).
- [Scott Daley](https://twitter.com/GravitonPunch) modified the shader to get it working with Unity ECS and the Hybrid Renderer.

## Compatibility

Test with:
- Unity 2020.3.30f1
- URP 10.8.1
- Entities 0.50.0-preview.24
- Hybrid Renderer 0.50.0-preview.24