using System;
using Nrjwolf.Tools.AttachAttributes;
using UnityEngine;

namespace Audio
{
    public class MusicSingleton : MonoBehaviour
    {
        public static MusicSingleton Instance;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        public static void ResetStatics()
        {
            Instance = null;
        }


        [GetComponent] [SerializeField]
        private AudioSource music;

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                gameObject.SetActive(false);
                return;
            }

            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            music.loop = true;
            music.Play();
        }
    }
}