using DG.Tweening;
using UnityEngine;

namespace Misc
{
    public class CameraRotate : MonoBehaviour
    {
        [SerializeField]
        private float duration = 2;
        
        
        private void Start()
        {
            transform.DORotate(Vector3.up * 180, duration * 2).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
        }
    }
}
