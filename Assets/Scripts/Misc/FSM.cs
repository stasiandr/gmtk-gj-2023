using System;
using NaughtyAttributes;
using UniRx;
using UnityEngine;

namespace Misc
{
    public abstract class FSM<T> : MonoBehaviour where T : Enum
    {
        [HideInInspector]
        public ReactiveProperty<(T old, T current)> state;

        [ShowNativeProperty]
        public T CurrentState => state.Value.current;
        
        protected abstract T DefaultState { get; }

        protected virtual void Awake()
        {
            state = new ReactiveProperty<(T old, T current)>((DefaultState, DefaultState));
        }

        protected void SetState(T newState)
        {
            state.Value = (state.Value.current, newState);
        }
    }
}