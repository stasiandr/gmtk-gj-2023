using System;
using UniRx;
using UnityEngine;

namespace Misc
{
    public class CameraRotator : MonoBehaviour
    {
        private void Start()
        {
            Observable.EveryFixedUpdate()
                .Subscribe(_ => transform.rotation = Camera.main!.transform.rotation)
                .AddTo(this);
        }
    }
}