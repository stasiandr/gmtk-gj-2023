using System;
using Gameplay;
using Nrjwolf.Tools.AttachAttributes;
using TNRD;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Misc
{
    public class HealthBar : MonoBehaviour
    {
        [GetComponentInParent]
        [SerializeField]
        private Damagable damagable;

        [SerializeField] [GetComponentInChildren()]
        private Slider slider;

        private void Start()
        {
            damagable.ObservableHealth
                .Subscribe(val => slider.value = val)
                .AddTo(this);
        }
    }
}