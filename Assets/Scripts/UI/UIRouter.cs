using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Nrjwolf.Tools.AttachAttributes;
using UnityEngine;

namespace UI
{
    public class UIRouter : MonoBehaviour
    {
        [GetComponentInChildren]
        public MainMenuController mainMenuController;
        
        [GetComponentInChildren]
        public SettingsController settingsController;
        
        [GetComponentInChildren]
        public SelectLevelController selectLevelController;

        private IEnumerable<UIControllerBase> Controllers =>
            new UIControllerBase[] { mainMenuController, settingsController, selectLevelController };


        public void Open(UIControllerBase controller) => UniTask.Create(async () =>
        {
            foreach (var controllerBase in Controllers)
            {
                if (controllerBase != controller)
                    await controllerBase.Close();
            }

            UniTask.Create(async () => await controller.Open());
        });
    }
}