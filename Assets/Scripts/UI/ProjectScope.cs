using System.Collections.Generic;
using Player;
using UI.SelectLevel;
using VContainer;
using VContainer.Unity;

namespace UI
{
    public class ProjectScope : LifetimeScope
    {
        public List<LevelData> levelData;

        protected override void Configure(IContainerBuilder builder)
        {
            builder.RegisterInstance(levelData);
            builder.Register<FunSystem>(Lifetime.Singleton);
        }
    }
}