using UnityEngine;

namespace UI
{
    public class MainMenuController : UIControllerBase
    {
        public void PlayButtonClick() => router.Open(router.selectLevelController);

        public void SettingsButtonClick() => router.Open(router.settingsController);

        public void ExitButtonClick() => Application.Quit();
    }
}
