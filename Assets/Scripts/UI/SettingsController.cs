using Cysharp.Threading.Tasks;
using UnityEngine;

namespace UI
{
    public class SettingsController : UIControllerBase
    {
        public void BackButtonClick() => router.Open(router.mainMenuController);

        public void CloseInGame() => UniTask.Create(Close);
        public void OpenInGame() => UniTask.Create(Open);
    }
}