using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Nrjwolf.Tools.AttachAttributes;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(Canvas))]
    [RequireComponent(typeof(CanvasGroup))]
    public class UIControllerBase : MonoBehaviour
    {
        private const float FadeDuration = 0.1f;

        [GetComponent]
        [SerializeField]
        private Canvas canvas;

        [GetComponentInParent]
        [SerializeField]
        protected UIRouter router;

        [GetComponent] [SerializeField] 
        protected CanvasGroup group;

        internal async UniTask Open()
        {
            var wasEnabled = canvas.enabled;
            canvas.enabled = true;
            if (!wasEnabled) await OpenAnimation();
        }

        protected virtual async UniTask OpenAnimation()
        {
            group.alpha = 0;
            await group.DOFade(1, FadeDuration);
            group.interactable = true;
        }

        protected virtual async UniTask CloseAnimation()
        {
            group.alpha = 1;
            await group.DOFade(0, FadeDuration);
            group.interactable = false;
        }

        internal async UniTask Close()
        {
            if (canvas.enabled) await CloseAnimation();
            canvas.enabled = false;
        }
    }
}