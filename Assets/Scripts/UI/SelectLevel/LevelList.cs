using System.Collections.Generic;
using UnityEngine;
using VContainer;

namespace UI.SelectLevel
{
    public class LevelList : MonoBehaviour
    {
        [SerializeField]
        private LevelViewController level;
        
        [SerializeField]
        private LevelViewController levelLocked;
        
        private List<LevelData> _levelData;


        [Inject]
        private void Construct(List<LevelData> levelData)
        {
            _levelData = levelData;
            
            level.gameObject.SetActive(false);
            levelLocked.gameObject.SetActive(false);

            GenerateLevelViews();
        }

        private void GenerateLevelViews()
        {
            for (var i = 0; i < _levelData.Count; i++)
            {
                var lvc = Instantiate(i == 0 || _levelData[i - 1].IsFinished ? level : levelLocked, transform);
                
                lvc.Build(_levelData[i]);
                lvc.gameObject.SetActive(true);
            }
        }
    }
}