using NaughtyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI.SelectLevel
{
    [CreateAssetMenu]
    public class LevelData : ScriptableObject
    {
        private string IsFinishedKey => $"{levelTag}_finished";
        public bool IsFinished
        {
            get => PlayerPrefs.HasKey(IsFinishedKey);
            set => PlayerPrefs.SetString(IsFinishedKey, "true");
        }

        public string levelTag;
        public string levelName;
        public Sprite image;

        [Scene]
        public string level;


        public int Time
        {
            get
            {
                var key = $"{levelTag}_time";
                if (!PlayerPrefs.HasKey(key)) return -1;

                return PlayerPrefs.GetInt(key);
            }
            set => PlayerPrefs.SetInt($"{levelTag}_time", value);
        }
        public int Score
        {
            get
            {
                var key = $"{levelTag}_score";
                if (!PlayerPrefs.HasKey(key)) return -1;

                return PlayerPrefs.GetInt(key);
            }
            set => PlayerPrefs.SetInt($"{levelTag}_score", value);
        }

        public void OpenLevel() => SceneManager.LoadScene(level);
    }
}