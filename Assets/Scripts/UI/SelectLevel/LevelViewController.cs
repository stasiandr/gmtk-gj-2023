using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.SelectLevel
{
    public class LevelViewController : MonoBehaviour
    {
        public Button openLevel;
        public TMP_Text levelName;
        public TMP_Text time;
        public TMP_Text score;
        public Image image;

        public void Build(LevelData levelData)
        {
            levelName.text = levelData.levelName;
            time.text = levelData.Time == -1 ? "--:--" : $"{levelData.Time / 60}:{levelData.Time % 60}";
            score.text = levelData.Score == -1 ? "--" : levelData.Score.ToString();
            image.sprite = levelData.image;
            openLevel.onClick.AddListener(levelData.OpenLevel);
        }
    }
}