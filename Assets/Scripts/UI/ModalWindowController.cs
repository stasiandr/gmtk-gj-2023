using Nrjwolf.Tools.AttachAttributes;
using UnityEngine;
using UnityEngine.Events;

namespace UI
{
    public class ModalWindowController : MonoBehaviour
    {
        [GetComponent]
        [SerializeField] 
        private Canvas canvas;

        public UnityEvent onYes;

        public void Open()
        {
            canvas.enabled = true;
        }


        public void Yes()
        {
            canvas.enabled = false;
            onYes.Invoke();
        }

        public void No()
        {
            canvas.enabled = false;
        }
    }
}