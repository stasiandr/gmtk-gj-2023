using Nrjwolf.Tools.AttachAttributes;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace UI.Settings
{
    public class MasterAudioSetting : MonoBehaviour
    {
        [GetComponentInChildren]
        [SerializeField]
        private Slider slider;
        
        [SerializeField]
        private AudioMixer audioMixer;
        

        private const string MasterAudio = "MasterAudio";

        private void Awake()
        {
            SetSetting(LoadSetting());
            
            slider.onValueChanged.AddListener(SetSetting);
        }


        private void SetSetting(float masterVolume)
        {
            PlayerPrefs.SetFloat(MasterAudio, masterVolume);

            audioMixer.SetFloat("MasterVolume", Mathf.Log(masterVolume) * 20);
            
            slider.SetValueWithoutNotify(masterVolume);
        }

        private float LoadSetting()
        {
            if (PlayerPrefs.HasKey(MasterAudio))
                return PlayerPrefs.GetFloat(MasterAudio);
            
            SetSetting(1);
            return LoadSetting();
        }
    }
}