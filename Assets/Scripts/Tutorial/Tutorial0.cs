using System;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Gameplay;
using TMPro;
using UniRx;
using UnityEngine;

namespace Tutorial
{
    public class Tutorial0 : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text text;

        [SerializeField] 
        private Pickable pickable;

        [SerializeField]
        private PlayerLoader loader;
        
        public void Start() => UniTask.Create(async () =>
        {
            text.alpha = 0;
            await text.DOFade(1, .5f).SetEase(Ease.InCubic);
            
            await UniTask.WaitUntil(() => Input.GetKeyDown(KeyCode.Space));
            await text.DOFade(0, .5f);
            text.text = "Now left click on orc and place hin on level";
            await text.DOFade(1, .5f);

            await pickable.pickedValue.WaitUntilValueChangedAsync();
            loader.blocked = false;

            await text.DOFade(0, .5f);
            text.text = "Watch out for boring meter. Player won't be bored when he is doing something";
            await text.DOFade(1, .5f);
            
        });
    }
}