using System;
using DG.Tweening;
using Nrjwolf.Tools.AttachAttributes;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Gameplay
{
    public class LoaderView : MonoBehaviour
    {
        [GetComponent] [SerializeField] private Slider slider;
        [GetComponent] [SerializeField] private CanvasGroup group;
        
        [SerializeField] private PlayerLoader loader;

        private void Start()
        {
            slider.value = 0;
            group.alpha = 0;
            group.DOFade(1, 1).SetEase(Ease.InCubic)
                .onComplete += () =>
            {
                loader.secondsRescaled
                    .Subscribe(val =>
                    {
                        slider.value = val;
                        if (slider.value > 0.9f) group.DOFade(0, 1).SetEase(Ease.OutCubic);
                    }).AddTo(this);
            };
        }
    }
}