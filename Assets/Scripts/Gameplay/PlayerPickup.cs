using Player;
using UnityEngine;
using VContainer;

namespace Gameplay
{
    public abstract class PlayerPickup : MonoBehaviour
    {
        protected FunSystem Fun;

        [Inject]
        public void Construct(FunSystem fun) => Fun = fun;

        public abstract void OnPickUp(MainCharacterBrains mainCharacter);
    }
}