using System;
using Misc;
using NaughtyAttributes;
using Nrjwolf.Tools.AttachAttributes;
using Player;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace Gameplay
{
    public class EnemyBrain : FSM<CharactersState>
    {
        protected override CharactersState DefaultState => CharactersState.Idle;

        [GetComponent] [SerializeField]
        private NavMeshAgent agent;

        [GetComponent]
        public Pickable pickable;
        
        [SerializeField] 
        private float attack;
        
        [SerializeField] 
        private float attackCooldown;

        private MainCharacterBrains _characterBrains;
        private float _attackTimer;

        private float _idleCooldown;

        public void OnEnable()
        {
            if (!LevelLoader.LevelLoaded) return;

            pickable.pickedValue
                .Where(val => val)
                .Where(_ => _characterBrains != null)
                .Subscribe(_ =>
                {
                    _characterBrains.EnemyWasPicked(this);
                }).AddTo(this);
            
            Observable.EveryUpdate()
                .Where(_ => _characterBrains != null)
                .Where(_ => !pickable.pickedValue.Value)
                .Where(_ => agent.enabled)
                .Subscribe(_ =>
                {
                    agent.SetDestination(_characterBrains.transform.position);
                })
                .AddTo(this);
            
            Observable.EveryUpdate().Subscribe(_ => _attackTimer -= Time.deltaTime).AddTo(this);
            Observable
                .EveryUpdate()
                .Where(_ => state.Value.current == CharactersState.Combat)
                .Where(_ => _characterBrains != null)
                .Where(_ => Vector3.Distance(transform.position, _characterBrains.transform.position) < 0.4f)
                .Where(_ => _attackTimer < 0)
                .Subscribe(_ =>
                {
                    Attack();
                    _attackTimer = attackCooldown;
                }).AddTo(this);
            
            Observable.EveryUpdate()
                .Where(_ => state.Value.current == CharactersState.Idle)
                .Where(_ => _characterBrains == null)
                .Where(_ => !pickable.pickedValue.Value)
                .Where(_ => _idleCooldown < 0)
                .Where(_ => agent.enabled)
                .Subscribe(_ =>
                {
                    agent.SetDestination(transform.position + Vector3.ProjectOnPlane(Random.insideUnitSphere, Vector3.up) * 1.5f);
                    
                    SetState(CharactersState.Walking);
                })
                .AddTo(this);
            
            Observable.EveryUpdate()
                .Where(_ => state.Value.current == CharactersState.Walking)
                .Where(_ => _characterBrains == null)
                .Where(_ => !pickable.pickedValue.Value)
                .Where(_ => agent.enabled)
                .Where(_ => agent.remainingDistance < agent.stoppingDistance + 0.1f)
                .Subscribe(_ =>
                {
                    _idleCooldown = Random.Range(1f, 5f);
                    SetState(CharactersState.Idle);
                })
                .AddTo(this);

            Observable.EveryUpdate().Subscribe(_ => _idleCooldown -= Time.deltaTime).AddTo(this);
        }
        
        private void Attack()
        {
            if (_characterBrains == null) return;
        
            _characterBrains.GetComponent<Damagable>().ReceiveDamage(attack, _characterBrains.transform.position);
        }

        
        public void NotifyBeingAttacked(MainCharacterBrains characterBrains)
        {
            _characterBrains = characterBrains;
            SetState(CharactersState.Combat);
        }

    }
}