using UnityEngine;

namespace Gameplay
{
    public class SimplePickUp : PlayerPickup
    {
        public GameObject spawnOnPickUp;
        
        public int healStrength;
        public int easy;
        public int boring = 1;
        
        public override void OnPickUp(MainCharacterBrains mainCharacter)
        {
            mainCharacter.GetComponent<Damagable>().Heal(healStrength, transform.position);
            Fun.AddEasy(easy, transform.position);
            Fun.AddFun(boring, transform.position);

            Instantiate(spawnOnPickUp, transform.position, Quaternion.identity);
            
            Destroy(gameObject);
        }
    }
}