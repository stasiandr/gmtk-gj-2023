using System;
using DG.Tweening;
using Player;
using UniRx;
using UnityEngine;
using UnityEngine.AI;
using VContainer;

namespace Gameplay
{
    public class Pickable : MonoBehaviour
    {
        [SerializeField] private Transform objectToOutline;
        [SerializeField] private MainCharacterBrains mainCharacter;
        
        
        private IDisposable _handle;
        private Plane _ground = new(Vector3.up, Vector3.zero);

        public BoolReactiveProperty pickedValue;
        private FunSystem _fun;

        private void Awake()
        {
            pickedValue = new BoolReactiveProperty();
        }

        [Inject]
        public void Construct(FunSystem fun)
        {
            _fun = fun;
        }

        private void OnEnable()
        {
            mainCharacter = FindObjectOfType<MainCharacterBrains>(true);

            pickedValue
                .Where(val => val)
                .Subscribe(_ =>
                {
                    if (TryGetComponent<NavMeshAgent>(out var navMeshAgent)) navMeshAgent.enabled = false;
                    if (TryGetComponent<EnemyBrain>(out var brain)) brain.enabled = false;
                    if (TryGetComponent<HealthPoitionAnim>(out var anim)) anim.enabled = false;
                    if (TryGetComponent<HumanoidAnimator>(out var humanoidAnimator)) humanoidAnimator.enabled = false;

                })
                .AddTo(this);
            
            
            pickedValue
                .Where(val => !val)
                .Subscribe(_ =>
                {
                    if (TryGetComponent<NavMeshAgent>(out var navMeshAgent)) navMeshAgent.enabled = true;
                    if (TryGetComponent<EnemyBrain>(out var brain)) brain.enabled = true;
                    if (TryGetComponent<HealthPoitionAnim>(out var anim)) anim.enabled = true;
                    if (TryGetComponent<HumanoidAnimator>(out var humanoidAnimator)) humanoidAnimator.enabled = true;
                })
                .AddTo(this);
        }
        
        public void Pick(Camera mainCamera)
        {
            CheckVisibility();

            _handle = Observable
            .EveryUpdate()
            .Subscribe(_ =>
            {
                var point = Position(mainCamera);

                transform.position = Vector3.Lerp(transform.position, point + Vector3.up * 1.5f, 0.1f);
            })
            .AddTo(this);
            
            
            pickedValue.Value = true;
        }

        public bool IsVisibleToPlayer()
        {
            if (!mainCharacter.enabled) return false;
            
            var targetPosition = mainCharacter.transform.position + Vector3.up * .2f;
            var myPosition = transform.position + Vector3.up * .2f;
            var mask = LayerMask.GetMask("Walls");

            return !Physics.Linecast(targetPosition, myPosition, mask);
        }

        public void CheckVisibility()
        {
            if (IsVisibleToPlayer()) _fun.AddUnfair(1, transform.position);
        }


        public void Place(Camera mainCamera)
        {
            _handle?.Dispose();

            transform.DOMove(Position(mainCamera), 0.1f).onComplete += () =>
            {
                pickedValue.Value = false;
                CheckVisibility();
            };
        }


        private void OnDisable()
        {
            _handle?.Dispose();
            _handle = null;
        }


        public Vector3 Position(Camera mainCamera)
        {
            var ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            _ground.Raycast(ray, out var enter);
            var point = ray.origin + ray.direction * enter;
            return point;
        }

        public void SetOutline(bool state)
        {
            objectToOutline.gameObject.layer = state ? LayerMask.NameToLayer("Outline") : LayerMask.NameToLayer("Default");
            foreach (Transform child in objectToOutline)
            {
                child.gameObject.layer = state ? LayerMask.NameToLayer("Outline") : LayerMask.NameToLayer("Default");
            }
        }
    }
}