using System;
using NaughtyAttributes;
using Nrjwolf.Tools.AttachAttributes;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Gameplay
{
    public class UnitHealth : Damagable
    {
        [GetComponent] [SerializeField]
        private AudioSource source;

        public AudioClip[] punches;
        
        public GameObject[] spawnOnDeath;

        public IntReactiveProperty CumulativeDamageTaken { get; set; }
        
        private void Awake()
        {
            Health = MaxHealth;
            ObservableHealth = new FloatReactiveProperty();
            ObservableHealth.Value = 1;
        }

        protected override void OnDeath()
        {
            gameObject.SetActive(false);
            
            var random = spawnOnDeath[Random.Range(0, spawnOnDeath.Length)];
            Instantiate(random, transform.position, Quaternion.identity);
            
            Destroy(gameObject);
        }

        public override bool ReceiveDamage(float value, Vector3 position)
        {
            source.Stop();
            source.clip = punches[Random.Range(0, punches.Length)];
            source.Play();
            
            return base.ReceiveDamage(value, position);
        }
    }
}