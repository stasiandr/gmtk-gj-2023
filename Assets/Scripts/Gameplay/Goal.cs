using System;
using Cysharp.Threading.Tasks;
using Player;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Gameplay
{
    public class Goal : MonoBehaviour
    {
        private void Start()
        {
            gameObject.OnCollisionEnterAsObservable()
                .Subscribe(col =>
                {
                    if (!col.collider.TryGetComponent<MainCharacterBrains>(out _)) return;

                    if (LevelManager.Instance != null) LevelManager.Instance.GoalReached();
                }).AddTo(this);
        }
    }
}