using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Misc;
using Nrjwolf.Tools.AttachAttributes;
using Player;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.AI;
using VContainer;

namespace Gameplay
{
    public class MainCharacterBrains : FSM<CharactersState>
    {
        protected override CharactersState DefaultState => CharactersState.Idle;

        [GetComponent] [SerializeField] 
        private NavMeshAgent agent;
        
        [GetComponent] [SerializeField]
        private UnitHealth health;
        
        [SerializeField] private LayerMask mask;

        private FunSystem _fun;

        [SerializeField] 
        private float attackCooldown = 1;
        [SerializeField] 
        private float attackStrength = 1;

        private float _attackTimer;
        
        private Transform _goal;
        
        private ReactiveProperty<Transform> _target;
        private ReactiveProperty<EnemyBrain> _enemy;
        private IDisposable _pickableHandle;

        private int _lostHP;

        [Inject]
        public void Construct(FunSystem fun)
        {
            _fun = fun;
        }

        protected override void Awake()
        {
            _goal = FindFirstObjectByType<Goal>().transform;
            _target = new ReactiveProperty<Transform>();
            _enemy = new ReactiveProperty<EnemyBrain>();
        }

        private void Start()
        {
            health.ObservableHealth
                .Where(val => val < .5f)
                .Subscribe(_ => _fun.AddHard(2, transform.position)).AddTo(this);
            
            Observable.EveryFixedUpdate()
                .Where(_ => _target.Value != null)
                .Where(_ => agent.enabled)
                .Subscribe(_ => agent.SetDestination(_target.Value.position)).AddTo(this);

            GetComponent<UnitHealth>().ObservableHealth
                .Where(val => val < 0.001f)
                .Where(_ => LevelManager.Instance != null)
                .Subscribe(_ => LevelManager.Instance.PlayerDead())
                .AddTo(this);

            TriggerAndCollisions();
            Combat();
        }

        private void Combat()
        {
            Observable.EveryUpdate().Subscribe(_ => _attackTimer -= Time.deltaTime).AddTo(this);
            _enemy.Where(val => val == null).Subscribe(_ => SetTarget(_goal)).AddTo(this);
            
            Observable.EveryUpdate()
                .Where(_ => state.Value.current == CharactersState.Combat)
                .Where(_ => Vector3.Distance(transform.position, _enemy.Value.transform.position) < 0.4f)
                .Where(_ => _attackTimer < 0)
                .Subscribe(_ =>
                {
                    Attack(_enemy.Value);
                    _attackTimer = attackCooldown;
                }).AddTo(this);
        }

        private void Attack(EnemyBrain enemy)
        {
            if (enemy.GetComponent<Damagable>().ReceiveDamage(attackStrength, enemy.transform.position))
            {
                _fun.AddFun(3, enemy.transform.position);
                if (health.ObservableHealth.Value > .5f) _fun.AddEasy(1, transform.position);
                _enemy.Value = null;
            }
        }

        private void TriggerAndCollisions()
        {
            var results = new Collider[100];
            Observable.EveryFixedUpdate()
                .Select(_ =>
                {
                    var size = Physics.OverlapSphereNonAlloc(transform.position, 2, results, mask);
                    if (size == 0) return null;

                    var minCollider = results[0];
                    for (int i = 0; i < size; i++)
                    {
                        var col = results[i];

                        if (col.TryGetComponent<EnemyBrain>(out var enemy) && !enemy.pickable.pickedValue.Value)
                            enemy.NotifyBeingAttacked(this);


                        if ((transform.position - col.transform.position).sqrMagnitude <
                            (transform.position - minCollider.transform.position).sqrMagnitude) minCollider = col;
                    }

                    return minCollider;
                }).Subscribe(col =>
                {
                    if (col == null) return;

                    if (col.TryGetComponent<PlayerPickup>(out var pickable) && !pickable.GetComponent<Pickable>().pickedValue.Value)
                    {
                        _pickableHandle = pickable.GetComponent<Pickable>().pickedValue
                            .Where(val => val)
                            .Subscribe(_ =>
                            {
                                _pickableHandle?.Dispose();
                                ResetTarget();
                            }).AddTo(this);
                        SetTarget(pickable.transform);
                    }
                    else if (col.TryGetComponent<EnemyBrain>(out var enemy) && !enemy.pickable.pickedValue.Value)
                    {
                        SetEnemy(enemy);
                    }
                }).AddTo(this);

            gameObject.OnCollisionEnterAsObservable()
                .Subscribe(col =>
                {
                    if (col.collider.TryGetComponent<PlayerPickup>(out var pickable))
                    {
                        pickable.OnPickUp(this);
                        ResetTarget();
                    }
                }).AddTo(this);
        }

        private void ResetTarget()
        {
            SetTarget(_goal);
        }

        private void SetEnemy(EnemyBrain enemy)
        {
            _enemy.Value = enemy;
            _target.Value = enemy.transform;
            _enemy.Value.NotifyBeingAttacked(this);
            SetState(CharactersState.Combat);
        }

        private void SetTarget(Transform target)
        {
            SetState(CharactersState.Walking);
            _target.Value = target;
        }

        public void EnemyWasPicked(EnemyBrain enemyBrain)
        {
            if (_enemy.Value == enemyBrain)
            {
                _enemy.Value = null;
                SetState(CharactersState.Walking);
            }
        }
    }
}