using DG.Tweening;
using Nrjwolf.Tools.AttachAttributes;
using Player;
using TMPro;
using UnityEngine;

namespace Gameplay
{
    [RequireComponent(typeof(TMP_Text))]
    public class TextFader : MonoBehaviour
    {
        [GetComponent] [SerializeField] private TMP_Text text;

        private void Start()
        {
            text.alpha = 0;
            text.DOFade(1, 1).SetEase(Ease.InCubic);

            if (LevelManager.Instance != null)
                LevelManager.Instance.onEndGame += () =>
                {
                    text.DOFade(0, 1).SetEase(Ease.InCubic);
                };
        }
    }
}