using System;
using UnityEngine;

namespace Gameplay
{
    public class HealthPoitionAnim : MonoBehaviour
    {
        public float positionSpeed = 0.1f;
        public float rotationSpeed = 10;
        
        
        private Vector3 _startPosition;

        private void OnEnable()
        {
            _startPosition = transform.position;
        }

        private void Update()
        {
            transform.position = _startPosition + Vector3.up * (Mathf.Sin(Time.timeSinceLevelLoad) * positionSpeed);
            transform.rotation *= Quaternion.Euler(0, Time.deltaTime * rotationSpeed, 0);
        }
    }
}