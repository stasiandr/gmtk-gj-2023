using Player;
using UniRx;
using UnityEngine;

namespace Gameplay
{
    public class Damagable : MonoBehaviour
    {
        public float Health { get; set; }
        
        [field: SerializeField]
        public float MaxHealth { get; set; }

        public FloatReactiveProperty ObservableHealth { get; set; }

        protected virtual void OnDeath() {}

        public virtual bool ReceiveDamage(float value, Vector3 position)
        {
            TextLogger.LogText($"-{value}", position, Color.red);
            
            Health -= value;
            ObservableHealth.Value = Health / MaxHealth;

            if (Health <= 0.01f)
            {
                OnDeath();
                return true;
            }

            return false;
        }


        public void Heal(float value, Vector3 position)
        {
            if (value < 0.01f) return;
            
            TextLogger.LogText($"-{value}", position, Color.green);
            
            Health += value;
            ObservableHealth.Value = Health / MaxHealth;
        }

    }
}