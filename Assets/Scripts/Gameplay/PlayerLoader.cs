using System;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Player;
using UniRx;
using UnityEngine;
using UnityEngine.AI;

namespace Gameplay
{
    public class PlayerLoader : MonoBehaviour
    {
        public float maxSeconds;
        private float _currentSeconds;

        public FloatReactiveProperty secondsRescaled;

        [SerializeField] public bool blocked;

        private void Awake() => UniTask.Create(async () =>
        {
            var mainCharacter = FindObjectOfType<MainCharacterBrains>();
            mainCharacter.enabled = false;
            mainCharacter.GetComponent<NavMeshAgent>().enabled = false;

            var startPosition = mainCharacter.transform.position;
            mainCharacter.transform.position += Vector3.up * 100;

            await UniTask.WaitWhile(() => blocked);

            _currentSeconds = maxSeconds + 2;
            while (_currentSeconds > 0)
            {
                _currentSeconds -= Time.deltaTime;
                secondsRescaled.Value = 1 - _currentSeconds / maxSeconds;
                await UniTask.Yield();
            }

            secondsRescaled.Value = 1;
            
            await mainCharacter.transform.DOMove(startPosition, 1).SetEase(Ease.OutCubic);
            
            LevelManager.Instance.StartLevel();

            mainCharacter.enabled = true;
            mainCharacter.GetComponent<NavMeshAgent>().enabled = true;
        });
    }
}