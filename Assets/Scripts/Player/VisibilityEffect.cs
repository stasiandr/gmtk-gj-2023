using System;
using Gameplay;
using Nrjwolf.Tools.AttachAttributes;
using UniRx;
using UnityEngine;

namespace Player
{
    public class VisibilityEffect : MonoBehaviour
    {
        [GetComponent] [SerializeField]
        private LineRenderer lr;
        
        private IDisposable _handle;
        private MainCharacterBrains _mainChar;
        private Pickable _followed;

        private void Start()
        {
            _mainChar = FindObjectOfType<MainCharacterBrains>(true);
            
            lr.positionCount = 2;
            lr.enabled = LevelManager.Instance.unfairVisible;
        }

        public void SetVisibilityCheck(Pickable outlined, bool visibilityCheck)
        {
            if (visibilityCheck) Follow(outlined);
            else Unfollow(outlined);
        }


        private void Follow(Pickable component)
        {
            if (component == _followed) return;
            _followed = component;

            _handle = Observable.EveryUpdate()
                .Subscribe(_ =>
                {
                    if (component == null || _mainChar == null)
                    {
                        Unfollow(null);
                        return;
                    }
                    
                    lr.SetPositions(new []
                    {
                        _mainChar.transform.position + Vector3.up * .2f,
                        component.transform.position + Vector3.up * .2f
                    });

                    var visible = component.IsVisibleToPlayer();
                    lr.material.color = visible ? new Color(1, 0, 0, .5f) : Color.clear;
                }).AddTo(this);
        }

        private void Unfollow(Pickable pickable)
        {
            _followed = null;
            _handle?.Dispose();
            lr.SetPositions(new []
            {
                Vector3.one * -1000, 
                Vector3.one * -1000
            });
        }
    }
}