using System;
using Nrjwolf.Tools.AttachAttributes;
using UniRx;
using UnityEngine;

namespace Player
{
    public class CameraMovement : MonoBehaviour
    {
        [GetComponentInChildren]
        [SerializeField]
        private Camera mainCamera;

        [SerializeField]
        private float cameraSpeed;

        [SerializeField]
        private float rotationSpeed = 15;
        
        private void Start()
        {
            Observable
                .EveryUpdate()
                .Subscribe(_ =>
                {
                    var movement = (Vector3.left + Vector3.forward) * Input.GetAxis("Vertical") +
                                   (Vector3.forward + Vector3.right) * Input.GetAxis("Horizontal");


                    transform.position += movement * Time.deltaTime * cameraSpeed;
                })
                .AddTo(this);
            
            Observable
                .EveryUpdate()
                .Subscribe(_ =>
                {
                    var angle = (float)(Input.GetKey(KeyCode.E) ? 1 : 0) + (Input.GetKey(KeyCode.Q) ? -1 : 0);
                    angle *= Time.deltaTime * rotationSpeed;


                    transform.rotation *= Quaternion.Euler(0, angle, 0);
                })
                .AddTo(this);



        }
    }
}