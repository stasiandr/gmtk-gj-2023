using System;
using System.Collections.Generic;
using Nrjwolf.Tools.AttachAttributes;
using UI.SelectLevel;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using VContainer;

namespace Player
{
    public class LevelManager : MonoBehaviour
    {
        public static LevelManager Instance => FindObjectOfType<LevelManager>();

        [GetComponentInChildren]
        public Canvas pauseCanvas;
        
        public WinScreenView winScreenView;
        
        
        public FunSystemSettings settings = FunSystemSettings.Default;


        public bool unfairVisible;
        public bool hardEasyVisible;

        private List<IDisposable> _updateHandle;
        private FunSystem _fun;

        private DateTime _timeStart;

        private LevelData _currentLevel;
        public event Action onEndGame;

        [Inject]
        public void Construct(FunSystem fun, List<LevelData> levelData)
        {
            _fun = fun;
            fun.LevelStart(settings);

            var index = levelData.FindIndex(l => l.level == SceneManager.GetActiveScene().name);
            _currentLevel = levelData[index];
            if (index + 1 < levelData.Count) winScreenView.nextLevel = levelData[index + 1];
            
            _updateHandle = new List<IDisposable>();
            
            Observable
                .EveryUpdate()
                .Where(_ => Input.GetKeyDown(KeyCode.Space))
                .Subscribe(_ =>
                {
                    SetPlay(Time.timeScale == 0);
                    
                }).AddTo(_updateHandle);

            Time.timeScale = 1;
        }

        private void SetPlay(bool pp)
        {
            Time.timeScale = pp ? 1 : 0;
            pauseCanvas.enabled = !pp;
        }


        public void StartLevel()
        {
            Observable.EveryUpdate()
                .Subscribe(_ => _fun.Update())
                .AddTo(_updateHandle);

            _timeStart = DateTime.Now;
        }

        private void OnDestroy()
        {
            foreach (var handles in _updateHandle) handles?.Dispose();
        }

        private void EndGame()
        {
//            Time.timeScale = 0;
            foreach (var handles in _updateHandle) handles?.Dispose();
            
            onEndGame?.Invoke();
            FindObjectOfType<LevelLoader>(true).Unload();
        }

        private int TimeSinceLevelStart => (DateTime.Now - _timeStart).Seconds;

        public void GameIsBoring()
        {
            EndGame();

            winScreenView.ShowScreen(new WinScreenView.WinScreenData
            {
                Header = "Level is too boring",
                Description = "Player were disappointed because he haven't found much amusements",
                Score = 0,
                Time = TimeSinceLevelStart,
            });
        }


        public void GoalReached()
        {
            EndGame();

            _currentLevel.IsFinished = true;
            _currentLevel.Score = Mathf.Max(_fun.CalculateScore(), _currentLevel.Score);
            _currentLevel.Time = _currentLevel.Time == -1 ? TimeSinceLevelStart : Mathf.Min(TimeSinceLevelStart, _currentLevel.Time);

            winScreenView.ShowScreen(new WinScreenView.WinScreenData
            {
                Header = "Player were satisfied",
                Description = "You made everything right!",
                Score = _fun.CalculateScore(),
                Time = TimeSinceLevelStart,
            }, true);
        }

        public void PlayerDead()
        {
            EndGame();

            winScreenView.ShowScreen(new WinScreenView.WinScreenData
            {
                Header = "Player dead!",
                Description = "You made player suffer. Next time give player more heal poitions, or less enemies.",
                Score = 0,
                Time = TimeSinceLevelStart,
            });
        }

        public void GameIsTooUnfair()
        {
            EndGame();

            winScreenView.ShowScreen(new WinScreenView.WinScreenData
            {
                Header = "Player suspects something",
                Description = "Player thinks that you are making up rules while he is playing. Try think ahead and place enemies more carefully.",
                Score = 0,
                Time = TimeSinceLevelStart,
            });
        }

        public void GameIsTooEasy()
        {
            EndGame();

            winScreenView.ShowScreen(new WinScreenView.WinScreenData
            {
                Header = "It's too easy",
                Description = "Easy peasy lemon squeezy, keep players hp below half and put traps. And may be don't put too many easy opponents.",
                Score = 0,
                Time = TimeSinceLevelStart,
            });
        }

        public void GameIsTooHard()
        {
            EndGame();

            winScreenView.ShowScreen(new WinScreenView.WinScreenData
            {
                Header = "It's too hard",
                Description = "Dark souls is kindergarteners' game compared to ours. Health potions, less enemies, I believe in you.",
                Score = 0,
                Time = TimeSinceLevelStart,
            });
        }
    }
}