using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Gameplay;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace Player
{
    [DefaultExecutionOrder(-100)]
    public class LevelLoader : MonoBehaviour
    {
        [SerializeField]
        private Transform floorTiles;
        
        [SerializeField]
        private Transform wallTiles;

        [SerializeField]
        private Transform other;

        [SerializeField]
        private List<Transform> enemies;

        public static bool LevelLoaded
        {
            get
            {
                var loader = FindObjectOfType<LevelLoader>(true);
                return loader == null || loader._levelLoaded;
            }
        }

        private bool _levelLoaded;
        
        private void Awake() => Load();

        private void Load() => UniTask.Create(async () =>
        {
            var minLoadTime = 0.1f * 3;
            var maxLoadTime = 0.25f * 3;
            var up = Vector3.up * 15;
            
            float Random() => UnityEngine.Random.Range(minLoadTime, maxLoadTime);

            foreach (Transform t in floorTiles) t.position += up;
            foreach (Transform t in wallTiles) t.position += up;
            foreach (Transform t in other) t.position += up;
            foreach (var t in enemies)
            {
                if (t.TryGetComponent<NavMeshAgent>(out var navMeshAgent)) navMeshAgent.enabled = false;
                if (t.TryGetComponent<EnemyBrain>(out var brain)) brain.enabled = false;
                if (t.TryGetComponent<HealthPoitionAnim>(out var anim)) anim.enabled = false;
                if (t.TryGetComponent<HumanoidAnimator>(out var humanoidAnimator)) humanoidAnimator.enabled = false;
                if (t.TryGetComponent<Pickable>(out var pickable)) pickable.enabled = false;
            
                t.position += up;
            }

            await UniTask.Delay((int)(1000 * maxLoadTime));
            
            foreach (Transform t in floorTiles)
                t.DOMove(t.position - up, Random()).SetEase(Ease.OutCubic);

            await UniTask.Delay((int)(1000 * maxLoadTime));
        
            foreach (Transform t in wallTiles)
                t.DOMove(t.position - up, Random()).SetEase(Ease.OutCubic);
        
            await UniTask.Delay((int)(1000 * maxLoadTime));
        
            foreach (Transform t in other)
                t.DOMove(t.position - up, Random()).SetEase(Ease.OutCubic);
        
            await UniTask.Delay((int)(1000 * maxLoadTime));
            
            _levelLoaded = true;
            
            foreach (var t in enemies)
            {
                t.DOMove(t.position - up, Random()).SetEase(Ease.OutCubic).onComplete += () =>
                {
                    if (t.TryGetComponent<NavMeshAgent>(out var navMeshAgent)) navMeshAgent.enabled = true;
                    if (t.TryGetComponent<EnemyBrain>(out var brain)) brain.enabled = true;
                    if (t.TryGetComponent<HealthPoitionAnim>(out var anim)) anim.enabled = true;
                    if (t.TryGetComponent<HumanoidAnimator>(out var humanoidAnimator)) humanoidAnimator.enabled = true;
                    if (t.TryGetComponent<Pickable>(out var pickable)) pickable.enabled = true;
                };
            }
        });
        
        
        public void Unload() => UniTask.Create(async () =>
        {
            var minLoadTime = 0.1f * 2;
            var maxLoadTime = 0.25f * 2;
            var up = Vector3.up * 15;
            
            float Random() => UnityEngine.Random.Range(minLoadTime, maxLoadTime);

            foreach (var t in enemies.Where(t => t != null))
            {
                if (t.TryGetComponent<NavMeshAgent>(out var navMeshAgent)) navMeshAgent.enabled = false;
                if (t.TryGetComponent<EnemyBrain>(out var brain)) brain.enabled = false;
                if (t.TryGetComponent<HealthPoitionAnim>(out var anim)) anim.enabled = false;
                if (t.TryGetComponent<HumanoidAnimator>(out var humanoidAnimator)) humanoidAnimator.enabled = false;
                if (t.TryGetComponent<Pickable>(out var pickable)) pickable.enabled = false;
            }
            
            
            await UniTask.Delay((int)(1000 * maxLoadTime));
            
            foreach (var t in enemies.Where(t => t != null))
                t.DOMove(t.position + up, Random()).SetEase(Ease.InCubic);

//            await UniTask.Delay((int)(1000 * maxLoadTime));
        
            foreach (Transform t in other)
                t.DOMove(t.position + up, Random()).SetEase(Ease.InCubic);
        
//            await UniTask.Delay((int)(1000 * maxLoadTime));
        
            foreach (Transform t in wallTiles)
                t.DOMove(t.position + up, Random()).SetEase(Ease.InCubic);
        
//            await UniTask.Delay((int)(1000 * maxLoadTime));

            foreach (Transform t in floorTiles)
                t.DOMove(t.position + up, Random()).SetEase(Ease.InCubic);
            
        });
        
        
    }
}