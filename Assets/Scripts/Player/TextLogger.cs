using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Player
{
    public class TextLogger : MonoBehaviour
    {
        public static TextLogger Instance;

        public TMP_Text prefab;
        
        private void Awake()
        {
            Instance = this;
            prefab.gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            Instance = null;
        }

        public static void LogText(string text, Vector3 position, Color color)
        {
            if (Instance == null) return;

            var go = Instantiate(Instance.prefab, position, Quaternion.identity);
            go.text = text;
            go.color = color;
            go.gameObject.SetActive(true);

            go.transform.DOMoveY(position.y + 3, 2.1f).SetEase(Ease.OutCubic);
            go.transform.DOMoveX(position.x + Random.value * 1.5f, 2f);
            go.transform.DOMoveZ(position.z + Random.value * 1.5f, 2f);
            
            go.DOColor(Color.clear, 2.5f).SetEase(Ease.OutCubic);
            Destroy(go.gameObject, 2.01f);
        }
    }
}