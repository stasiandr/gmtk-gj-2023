using Nrjwolf.Tools.AttachAttributes;
using TMPro;
using UI.SelectLevel;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Player
{
    public class WinScreenView : MonoBehaviour
    {
        [SerializeField] [GetComponent] 
        private Canvas canvas;

        [SerializeField] [GetComponent] 
        private AudioSource audioSource;
        
        public TMP_Text header;
        public TMP_Text description;
        public TMP_Text time;
        public TMP_Text score;

        public Button nextLevelButton;
        public Button exitToMenu;
        
        public struct WinScreenData
        {
            public string Header;
            public string Description;

            public int Time;
            public int Score;
        }

        public UnityEvent onScreenShow;

        public LevelData nextLevel;
        public LevelData mainMenu;


        public AudioClip winSound;
        public AudioClip looseSound;

        public void ShowScreen(WinScreenData data, bool nextLevel = false)
        {
            header.text = data.Header;
            description.text = data.Description;
            time.text = $"{data.Time / 60}:{data.Time % 60}";
            score.text = data.Score.ToString();

            nextLevelButton.GetComponentInChildren<TMP_Text>().text = nextLevel ? "Next level" : "Restart";
            
            if (this.nextLevel != null && nextLevel)
                nextLevelButton.onClick.AddListener(() => this.nextLevel.OpenLevel());
            else if (!nextLevel) 
                nextLevelButton.onClick.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().name));
            else nextLevelButton.onClick.AddListener(() => mainMenu.OpenLevel());
            
            exitToMenu.onClick.AddListener(() =>
            {
                mainMenu.OpenLevel();
                Time.timeScale = 1;
            });

            onScreenShow.Invoke();

            audioSource.Stop();
            audioSource.clip = nextLevel ? winSound : looseSound;
            audioSource.Play();

            canvas.enabled = true;
        }
    }
}
