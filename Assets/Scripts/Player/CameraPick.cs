using System;
using Gameplay;
using Nrjwolf.Tools.AttachAttributes;
using UniRx;
using UnityEngine;

namespace Player
{
    public class CameraPick : MonoBehaviour
    {
        [GetComponent]
        [SerializeField]
        private Camera mainCamera;

        [SerializeField] 
        private LayerMask mask;

        [SerializeField] private AudioSource pickAudio;
        [SerializeField] private AudioSource placeAudio;


        private Pickable _pickable;

        public PickEffect pickEffect;
        public VisibilityEffect visibility;
        private void Start()
        {
            Observable
                .EveryUpdate()
                .Where(_ => Input.GetMouseButtonDown(0))
                .Where(_ => Time.timeScale != 0)
                .Subscribe(_ =>
                {
                    if (_pickable == null)
                    {
                        if (!Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out var hit, 10000,
                                mask)) return;
                        if (!hit.transform.TryGetComponent<Pickable>(out var pickable)) return;

                        pickable.Pick(mainCamera);
                        pickEffect.Follow(pickable);
                        _pickable = pickable;
                        
                        pickAudio.Stop();
                        pickAudio.Play();
                        
                    }
                    else
                    {
                        _pickable.Place(mainCamera);
                        pickEffect.Unfollow(_pickable);
                        _pickable = null;
                        
                        placeAudio.Stop();
                        placeAudio.Play();
                    }
                })
                .AddTo(this);


            Pickable outlined = null;

            Observable.EveryUpdate()
                .Select(_ => Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out var hit, 10000, mask) ? hit.transform : null)
                .Select(val => val == null ? null : val.GetComponent<Pickable>())
                .Subscribe(val =>
                {
                    if (outlined != null && outlined != val)
                    {
                        outlined.SetOutline(false);
                        visibility.SetVisibilityCheck(outlined, false);
                    }
                    
                    if (val == null ) visibility.SetVisibilityCheck(outlined, false);
                    
                    outlined = val;
                    if (outlined != null)
                    {
                        outlined.SetOutline(true);
                        visibility.SetVisibilityCheck(outlined, true);
                    }
                })
                .AddTo(this);
        }
        
        
        
    }
}