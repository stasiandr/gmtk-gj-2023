using System;
using UniRx;
using UnityEngine;

namespace Player
{
    public class FunSystem
    {
        private FunSystemSettings _settings;

        public FloatReactiveProperty HardEasy = new();
        public FloatReactiveProperty BoringFun = new();
        public IntReactiveProperty Unfair = new();

        public IntReactiveProperty MaxUnfair = new ();

        public int CalculateScore()
        {
            return (int)((1 - Mathf.Abs(HardEasy.Value)) * 10 + -Unfair.Value * 100 + (1 - BoringFun.Value) * 1000);
        }
        
        private int _hardEasy;
        private float _boringFun;
        private int _unfair;
        
        public void LevelStart(FunSystemSettings settings)
        {
            _settings = settings;

            _boringFun = 0;
            _hardEasy = 0;
            _unfair = 0;

            UpdateProps();
        }

        private void UpdateProps()
        {
            HardEasy.Value = (float)_hardEasy / _settings.MaxHardEasy;
            BoringFun.Value = _boringFun / _settings.MaxBoring;
            Unfair.Value = _unfair;
            MaxUnfair.Value = _settings.MaxUnfair;
        }

        public void Update()
        {
            _boringFun += _settings.BoringSpeed * Time.deltaTime;
            
            UpdateProps();
            
            if (_boringFun >= _settings.MaxBoring)
                GameIsTooBoring();
        }
        
        
        public void AddFun(float points, Vector3 position)
        {
            _boringFun = Mathf.Max(_boringFun - points, 0);
            
            UpdateProps();
            
            TextLogger.LogText($"-{points} Boring", position, Color.white);
        }

        public void AddEasy(int points, Vector3 position)
        {
            _hardEasy += points;
            UpdateProps();

            if (LevelManager.Instance != null && !LevelManager.Instance.hardEasyVisible) return;

            TextLogger.LogText($"+{points} Easy", position, Color.white);
            
            if (_hardEasy >= _settings.MaxHardEasy)
                GameIsTooEasy();
        }


        public void AddHard(int points, Vector3 position)
        {
            _hardEasy -= points;
            UpdateProps();
            
            if (LevelManager.Instance != null && !LevelManager.Instance.hardEasyVisible) return;

            TextLogger.LogText($"+{points} Hard", position, Color.white);

            if (_hardEasy <= -_settings.MaxHardEasy)
                GameIsTooHard();
        }


        public void AddUnfair(float points, Vector3 position)
        {
            _unfair++;
            UpdateProps();
            
            if (LevelManager.Instance != null && !LevelManager.Instance.unfairVisible) return;
            
            TextLogger.LogText($"+{points} Unfair", position, Color.white);


            if (_unfair >= _settings.MaxUnfair) GameIsTooUnfair();
        }
        
        
        private void GameIsTooBoring()
        {
            if (LevelManager.Instance != null) LevelManager.Instance.GameIsBoring();
        }
        
        private void GameIsTooEasy()
        {
            if (LevelManager.Instance != null) LevelManager.Instance.GameIsTooEasy();
        }
        
        private void GameIsTooHard()
        {
            if (LevelManager.Instance != null) LevelManager.Instance.GameIsTooHard();
        }

        private void GameIsTooUnfair()
        {
            if (LevelManager.Instance != null) LevelManager.Instance.GameIsTooUnfair();
        }
    }



    [Serializable]
    public struct FunSystemSettings
    {
        public static FunSystemSettings Default => new FunSystemSettings
        {
            BoringSpeed = 1,
            MaxBoring = 100,
            MaxHardEasy = 10,
            MaxUnfair = 3,
        };

        public int MaxUnfair;

        public float BoringSpeed;
        public float MaxBoring;
        public int MaxHardEasy;
    }
    
}