using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using VContainer;

namespace Player
{
    public class StatsView : MonoBehaviour
    {
        public Slider hardEasy;
        public Slider boring;
        public Image[] unfair;


        [Inject]
        public void Construct(FunSystem fun)
        {
            fun.BoringFun.Subscribe(val => boring.value = val).AddTo(this);
            fun.HardEasy.Subscribe(val => hardEasy.value = val).AddTo(this);
            fun.Unfair.Subscribe(val =>
            {
                for (int i = 0; i < unfair.Length; i++) unfair[i].color = i < val ? Color.red : Color.white;
            }).AddTo(this);

            fun.MaxUnfair.Subscribe(val =>
            {
                for (int i = 0; i < unfair.Length; i++) unfair[i].gameObject.SetActive(i < val);
            }).AddTo(this);
        }

    }
}