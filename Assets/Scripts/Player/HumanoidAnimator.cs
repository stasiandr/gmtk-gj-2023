using System;
using System.Diagnostics.CodeAnalysis;
using Gameplay;
using Misc;
using UnityEngine;

namespace Player
{
    [SuppressMessage("ReSharper", "Unity.InefficientMultiplicationOrder")]
    public class HumanoidAnimator : MonoBehaviour
    {
        public Transform body;

        public float animationSpeed;
        
        private FSM<CharactersState> _fsm;

        private Vector3 _bodyStartLocalPosition;

        private Vector3 _position;
        private float _acc;

        private void Start()
        {
            _fsm = GetComponent<FSM<CharactersState>>();
            _bodyStartLocalPosition = body.localPosition;
            _position = transform.position;
        }

        private void Update()
        {
            var positionDelta = (transform.position - _position).sqrMagnitude;
            _position = transform.position;

            _acc += positionDelta * 100 * animationSpeed;
            
            UnpackState(WalkingState(_acc));
            
            
            switch (_fsm.state.Value.current)
            {
                case CharactersState.Idle:
                    UnpackState(IdleState());
                    break;
            }
        }

        

        private void UnpackState((Quaternion rot, Vector3 pos) state)
        {
            body.transform.localRotation = state.rot;
            body.transform.localPosition = state.pos;
        }

        private (Quaternion rot, Vector3 pos) IdleState()
        {
            var lerpAlpha = 0.1f;
            return (Quaternion.Slerp(body.localRotation, Quaternion.identity, lerpAlpha), Vector3.Lerp(body.localPosition, _bodyStartLocalPosition, lerpAlpha));
        }

        private (Quaternion rot, Vector3 pos) WalkingState(float time)
        {
            return (Quaternion.Slerp(Quaternion.Euler(0, 0, -15), Quaternion.Euler(0, 0, 15), Mathf.Abs(time % 1 - .5f) * 2), 
                _bodyStartLocalPosition +
                (.5f - Mathf.Abs(time * 2 % 1 - .5f)) * (Vector3.up * .2f)
                + (Mathf.Abs(time % 1 - .5f) * 4 - 2) * (Vector3.left * .05f));
        }
    }
}