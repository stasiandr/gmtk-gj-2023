using System;
using Cysharp.Threading.Tasks;
using Gameplay;
using Nrjwolf.Tools.AttachAttributes;
using UniRx;
using UnityEngine;

namespace Player
{
    public class PickEffect : MonoBehaviour
    {
        [GetComponent] [SerializeField]
        private ParticleSystem ps;
        
        private IDisposable _handle;

        public void Follow(Pickable component)
        {
            ps.Stop();
            ps.Play();
            
            _handle = Observable.EveryUpdate()
                .Subscribe(_ =>
                {
                    
                    transform.position = component.Position(Camera.main);



                }).AddTo(this);
        }

        public void Unfollow(Pickable pickable)
        {
            ps.Stop();
            _handle?.Dispose();
            transform.position = Vector3.one * -1000;
        }
    }
}