using System;

// This file is auto-generated. Do not modify or move this file.

public static class BuildConstants
{
    public enum ReleaseType
    {
        None,
        dev,
        prod,
    }

    public enum Platform
    {
        None,
        macOS,
        PC,
    }

    public enum Architecture
    {
        None,
        macOS,
        Windows_x64,
    }

    public enum Distribution
    {
        None,
    }

    public static readonly DateTime buildDate = new DateTime(638245094264094260);
    public const string version = "1.0.0.1";
    public const ReleaseType releaseType = ReleaseType.dev;
    public const Platform platform = Platform.macOS;
    public const Architecture architecture = Architecture.macOS;
    public const Distribution distribution = Distribution.None;
}

